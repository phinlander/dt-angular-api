<?php
  require_once __DIR__."/vendor/autoload.php";
  DTSettingsConfig::initShared(__DIR__."/../local/config.json");
  DTSettingsStorage::initShared(__DIR__."/../local/storage.json");