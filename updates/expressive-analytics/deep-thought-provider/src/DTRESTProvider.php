<?php
/**
 * DTRESTProvider
 *
 * Copyright (c) 2013-2016, Expressive Analytics, LLC <info@expressiveanalytics.com>.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @package    Deep Thought (Provider)
 * @author     Blake Anderson <blake@expressiveanalytics.com>
 * @copyright  2013-2014 Expressive Analytics, LLC <info@expressiveanalytics.com>
 * @license    http://choosealicense.com/licenses/mit
 * @link       http://www.expressiveanalytics.com/
 * @since      version 1.0.0
 */

class DTRESTProvider extends DTProvider{
	/**
	performs an action by name
	@param action - the action to perform (uses 'act' param key, if null)
	@note if action is the name of a method, the appropriate method is called
	*/
	protected function performAction($action=null){
		try{
			$method = strtoupper($_SERVER["REQUEST_METHOD"]);
			$this->setResponse($this->handleREST($action,$method));
		}catch(Exception $e){
			if($e->getCode()==404) //not a RESTful action, use the default behavior
				parent::performAction($action);
		}
	}
		
	protected function handleREST($action=null,$method){
		$f = $action;
		
		if($this->actionExists($f)) // we're already defined, just do it
			return $this->$f();

		// determine whether this is a collection or individual entity
		$id = $this->params->stringParam(static::$lookup_column);
		$is_collection = empty($id);
				
		switch($method){
			case "DELETE": // delete a resource
				if($is_collection)
					return $this->actionRemoveAll();
				else
					return $this->actionRemove();
			case "POST": // create a resource
				if($is_collection)
					return $this->actionCreateMany();
				else
					return $this->actionCreate();
			case "PUT": // replace/create a resource
				if($is_collection){
					$this->actionRemoveAll();
					$this->actionCreateMany();
				}else
					return $this->actionUpsert();
			case "PATCH": // update an existing resource
				if($is_collection)
					return $this->updateMany();
				else
					return $this->update();
			case "GET": // retrieve a resource
				if($is_collection) 
					return $this->actionList();
				else
					return $this->actionGet();
		}
		throw new Exception("non-RESTful action",404);
	}
}