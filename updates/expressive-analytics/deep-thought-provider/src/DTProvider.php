<?php
/**
 * DTProvider
 *
 * Copyright (c) 2013-2014, Expressive Analytics, LLC <info@expressiveanalytics.com>.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @package    Deep Thought (Provider)
 * @author     Blake Anderson <blake@expressiveanalytics.com>
 * @copyright  2013-2014 Expressive Analytics, LLC <info@expressiveanalytics.com>
 * @license    http://choosealicense.com/licenses/mit
 * @link       http://www.expressiveanalytics.com/
 * @since      version 1.0.0
 */


class DTProvider{
	protected static $model; // the default model to use for automatic actions
	/** 
		determines which automatic actions are available, defaults to "read-only"
		valid values include: get, list, count, create, create_many,
		update, update_many, upsert, remove, remove_all
	*/
	protected static $automatic_actions = array("list","get","count");
	protected static $archive_column; // flag this column instead of deleting rows
	protected static $lookup_column; // unique "key" column for automatic actions, defaults to model's primary key
	protected static $creation_column; // defines a column to store a creation timestamp
	protected static $modified_column; // defines a column to store a modified timestamp
	
	protected $params = null;
	public $session = null;
	protected $response = null;
	public $verifier = null;
	
	function __construct(DTVerifier $verifier=null,DTStore $db=null){
		$this->db = isset($db)?$db:DTSettingsStorage::defaultStore();
		$this->setParams($_REQUEST);
		$token = $this->params->stringParam("tok");
		$this->verifier = isset($verifier)?$verifier:new DTBasicVerifier($this->db,$token);
		$this->response = new DTResponse();
		$this->session = DTSession::sharedSession();
		
		// make sure we have populated the lookup column
		if(isset(static::$model)){
			$model = static::$model;
			static::$lookup_column = empty(static::$lookup_column)?$model::primaryKeyColumn():static::$lookup_column;
		}
	}
	
	function actionSessionDestroy(){
		DTSession::destroy();
	}
	
	public function setParams(array $params){
		$this->params = new DTParams($params,$this->db);
	}
	
//==================
//! Request Handling
//==================
/** @name Request Handling
 *  Session setup and action delivery
 */
///@{
	/**
		A convenience method for handling a standard request and sending a response
	*/
	public function handleRequest(){
		$act = $this->params->stringParam("act");
		if(empty($act)){ //try a routing style
			if(preg_match("/\.php\/([^\?]*)/",$_SERVER["PHP_SELF"],$matches)){
				$route=$matches[1];
				$parts = explode("/", $route);
				$act = $parts[0];
			}
		}
		$action = "action".preg_replace('/[^A-Z^a-z^0-9]+/','',$act);
		if($this->verifyRequest($action))
			$this->performAction($action);
		else
			$this->setResponseCode(DT_ERR_PROHIBITED_ACTION);
		$this->response->respond($this->params->allParams());
		$this->recordRequest();
	}
	
	public function verifyRequest($action){
		return $this->verifier->verify($action);
	}

	/**
	performs an action by name
	@param action - the action to perform (uses 'act' param key, if null)
	@note if action is the name of a method, the appropriate method is called
	*/
	protected function performAction($action=null){
		$action = (isset($action)?$action:$this->params->stringParam("act"));
		if($this->actionExists($action))
			$this->setResponse($this->$action());
		else
			DTLog::warn("Action not found ({$action}): ".$e->getMessage());
	}
	
	public function setResponse($response){
		$this->response->setResponse($response);
	}
	
	public function responseCode(){
		return $this->response->error();
		
	}
	
	/** @return returns null so that it may be chained with an action's return statement */
	public function setResponseCode($code){
		$this->response->error($code);
		return null;
	}
///@}
	
	protected function recordRequest(){
		try{
			$recording_db = DTSettingsStorage::connect("logs");
			//@todo write to logs db
		}catch(Exception $e){}
	}
	
	/**
		modifies a QB to be paginated by request parameters +page+ and +count+
		@note page numbers start at 1
	*/
	protected function paginate(DTQueryBuilder $qb){
		$page = $this->params->intParam("page",1)-1;
		$count = $this->params->intParam("count",10);
		return $qb->limit("{$count} OFFSET ".$page*$count);
	}
	
	/** @internal */
	protected function actionExists($action){
		try {
			$meth = new ReflectionMethod($this,$action);
			if(method_exists($this, $action) && $meth->isPublic())
				return true;
		}catch(Exception $e) { }
		return false;
	}
	
//====================
//! Automatic Actions
//====================
/** @name Automatic Actions
 *  Default behavior for providers with a defined model
 */
///@{
	
	/** retrieves a single record by lookup value */
	public function actionGet(){
		if(!in_array("get",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$model = static::$model;
		$filter = array(static::$lookup_column=>$this->params->stringParam(static::$lookup_column));
		try{
			return new $model($this->db->filter($filter));
		}catch(Exception $e){}
		return null;
	}
	
	/** retrieves many records -- override to paginate/filter/sort */
	public function actionList(){
		if(!in_array("list",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$model = static::$model;
		return $model::select($this->db->filter());
	}
	
	/** retrieves the number of available records */
	public function actionCount(){
		if(!in_array("count",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$model = static::$model;
		return $model::count($this->db->filter());
	}
		
	/**
		create a new record--fails if record already exists
	*/
	public function actionCreate(){
		if(!in_array("create",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$model = static::$model;
		$filter = array( static::$lookup_column => $this->params->stringParam(static::$lookup_column) );
		try{
			$obj = new $model($this->db->filter($filter));
			throw new Exception ("conflict: object already exists", 409);
		}catch(Exception $e){
			$params = $this->params->allParams();
			$creation_params = array();
			if(!empty(static::$creation_column)) // use the creation timestamp
				$creation_params[static::$creation_column] = $this->db->now();
			$model::upsert($this->db->filter($filter),$params,$creation_params);
		}
	}
	
	/**
		assumes the input is an array of records to create and calls actionCreate for each
	*/
	public function actionCreateMany(){
		if(!in_array("create_many",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$params = $this->params->params; //get raw, dirty params
		foreach($params as $p){
			$this->setParams($p);
			$this->actionCreate();
		}
		$this->setParams($params); //restore the original params
	}
	
	/** update an existing record */
	public function actionUpdate(){
		if(!in_array("update",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$model = static::$model;
		try{
			$params = $this->params->allParams();
			if(!empty(static::$modified_column)) // use the modified timestamp
				$params[static::$modified_column] = $this->db->now();
			$filter = array( static::$lookup_column => $this->params->stringParam(static::$lookup_column) );
			$obj = new $model($this->db->filter($filter));
			$obj->clean();
			$obj->merge($params);
			$obj->update();
			return $obj;
		}catch(Exception $e){
			throw new Exception("record does not exist",404);
		}
	}
	
	/** update many existing records */
	public function actionUpdateMany(){
		if(!in_array("update_many",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$params = $this->params->params; //get raw, dirty params
		$objs = array();
		foreach($params as $p){
			$this->setParams($p);
			$objs[] = $this->actionUpdate();
		}
		$this->setParams($params); //restore the original params
		return $objs;
	}
	
	/**
		update a record or create it, if necessary
	*/
	public function actionUpsert(){
		if(!in_array("upsert",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$model = static::$model;
		$filter = $this->params->allParams();
		$creation_params = array();
		if(!empty(static::$creation_column)) // use the creation timestamp
			$creation_params[static::$creation_column] = $this->db->now();
		if(!empty(static::$modified_column)) // use the modified timestamp
			$params[static::$modified_column] = $this->db->now();
		$model::upsert($this->db->filter($filter),$filter,$creation_params);
	}
	
	/**
		remove one or many resources by lookup column value
	*/
	public function actionRemove(){
		if(!in_array("remove",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$model = static::$model;
		if(empty($model))
			throw new Exception ("method not implemented");
		$ids = $this->params->arrayParam(static::$lookup_column);
		
		$filter = array( static::$lookup_column => array("IN",$ids) );
		if(isset(static::$archive_column))
			$model::updateRows($this->db->filter($filter),array(static::$archive_column=>1));
		else
			$model::deleteRows($this->db->filter($filter));
	}
	
	/** complete, unfiltered removal of a model's storage */
	public function actionRemoveAll(){
		if(!in_array("remove_all",static::$automatic_actions))
			throw new Exception ("unauthorized action",401);
		$model = static::$model;
		$filter = array();
		if(isset(static::$archive_column))
			$model::updateRows($this->db->filter($filter),array(static::$archive_column=>1));
		else
			$model::deleteRows($this->db->filter($filter));
	}
	
///@}
}