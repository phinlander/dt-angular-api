<?php
  class BasePlanet extends DTModel {
    protected static $storage_table = "planet";

    public $name;
  }

  class Planet extends BasePlanet {
    protected static $has_many_manifest = array(
      "animals" => array("PlanetHasAnimal","PlanetAnimal")
    );
    public $animals;
  }

  class AnimalPlanet extends BasePlanet { }