<?php
  class PlanetHasAnimal extends DTModel {
    protected static $storage_table = "planet_has_animal";

    protected static $has_a_manifest = array(
      "planet" => array("Planet","planet_id"),
      "animal" => array("AnimalPlanet","animal_id")
    );
  }
  class AnimalHasPlanet extends DTModel {
    protected static $storage_table = "planet_has_animal";

    protected static $has_a_manifest = array(
      "planet" => array("PlanetAnimal","planet_id"),
      "animal" => array("Animal","animal_id")
    );
  }